package mad.citybin.backend.util.valiidator


fun isNull(obj: Any?): Boolean = obj == null
fun isNotNull(obj: Any?): Boolean = obj != null