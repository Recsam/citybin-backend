package mad.citybin.backend.data

import mad.citybin.backend.data.entity.Role
import mad.citybin.backend.data.repository.jpa.RoleJpaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import kotlin.system.exitProcess

@Component
class DatabaseSeeder @Autowired constructor(
        val roleJpaRepository: RoleJpaRepository
) {

    /**
     *  Register the seeders here
     *
     */
    @EventListener
    fun seed(event: ContextRefreshedEvent) {
        rolesTableSeeder()
    }

    /**
     *  Seeding roles table
     */
    private fun rolesTableSeeder() {
        if (roleJpaRepository.findAll().isNotEmpty())
            return

        roleJpaRepository.deleteAll()
        val userRole = Role()
        userRole.name = "user"
        val driverRole = Role()
        driverRole.name = "driver"
        val adminRole = Role()
        adminRole.name = "admin"
        roleJpaRepository.saveAll(listOf(userRole, driverRole, adminRole))
    }

}

