package mad.citybin.backend.data.entity

import javax.persistence.*

data class Village (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var name: String = "",

        @ManyToOne
        @JoinColumn(name = "commune_id")
        var commune: Commune = Commune()
)