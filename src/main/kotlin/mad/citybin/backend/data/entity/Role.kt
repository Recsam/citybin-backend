package mad.citybin.backend.data.entity

import javax.persistence.*

@Entity
@Table(name = "roles")
data class Role (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0,

    var name: String = ""

)