package mad.citybin.backend.data.entity

import javax.persistence.*

@Entity
@Table(name = "communes")
data class Commune (

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var name: String = "",

        @ManyToOne
        @JoinColumn(name = "province_id")
        var province: Province = Province()

)