package mad.citybin.backend.data.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "complaints")
data class Complaint (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        @ManyToOne
        @JoinColumn(name = "created_by")
        var creator: User = User(),

        @ManyToOne
        @JoinColumn(name = "bin_id")
        var bin: Bin = Bin(),

        var text: String = "",

        @CreationTimestamp
        var createdAt: Timestamp = Timestamp(System.currentTimeMillis()),

        @UpdateTimestamp
        var updatedAt: Timestamp = Timestamp(System.currentTimeMillis())

)