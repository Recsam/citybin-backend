package mad.citybin.backend.data.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var username: String = "",
        var email: String = "",
        var password: String = "",
        var tel: String = "",

        @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY)
        var complaints: List<Complaint> = emptyList(),

        @ManyToOne
        @JoinColumn(name = "role_id", nullable = false)
        var role: Role = Role(),

        // For Soft Deletes
        var isDeleted: Boolean = false,

        @CreationTimestamp
        var createdAt: Timestamp = Timestamp(System.currentTimeMillis()),

        @UpdateTimestamp
        var updatedAt: Timestamp = Timestamp(System.currentTimeMillis())

)