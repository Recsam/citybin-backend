package mad.citybin.backend.data.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "driver_details")
data class DriverDetail (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        @OneToOne
        @JoinColumn(name = "user_id")
        var user: User = User(),

        @OneToOne
        @JoinColumn(name = "created_by")
        var createdBy: User = User(),

        // For Soft Deletes
        var isDeleted: Boolean = false,

        @CreationTimestamp
        var createdAt: Timestamp = Timestamp(System.currentTimeMillis()),

        @UpdateTimestamp
        var updatedAt: Timestamp = Timestamp(System.currentTimeMillis())

)