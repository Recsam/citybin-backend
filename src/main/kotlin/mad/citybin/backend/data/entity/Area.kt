package mad.citybin.backend.data.entity

import javax.persistence.*

@Entity
@Table(name = "areas")
data class Area (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var name: String = ""
)