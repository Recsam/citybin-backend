package mad.citybin.backend.data.entity

import javax.persistence.*

@Entity
@Table(name = "bins")
data class Bin(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var name: String = "",

        @OneToOne
        @JoinColumn(name = "location_id")
        var location: Location = Location()
)