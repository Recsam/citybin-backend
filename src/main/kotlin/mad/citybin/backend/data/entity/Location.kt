package mad.citybin.backend.data.entity

import org.hibernate.annotations.CreationTimestamp
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "locations")
data class Location (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var latitude: Double = 0.0,
        var longitude: Double = 0.0,
        var zoom: String = "",

        @CreationTimestamp
        var createdAt: Timestamp = Timestamp(System.currentTimeMillis())
)