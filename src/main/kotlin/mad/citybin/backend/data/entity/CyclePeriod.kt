package mad.citybin.backend.data.entity

import java.sql.Time
import javax.persistence.*

@Entity
@Table(name = "cycle_periods")
data class CyclePeriod(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var period: Time = Time(0)
)