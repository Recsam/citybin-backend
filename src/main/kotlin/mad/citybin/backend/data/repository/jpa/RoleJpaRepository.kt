package mad.citybin.backend.data.repository.jpa

import mad.citybin.backend.data.entity.Role
import org.springframework.data.jpa.repository.JpaRepository

interface RoleJpaRepository : JpaRepository<Role, Int> {
    fun findByName(name: String): Role
}