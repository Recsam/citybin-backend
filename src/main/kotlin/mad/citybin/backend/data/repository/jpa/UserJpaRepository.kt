package mad.citybin.backend.data.repository.jpa

import mad.citybin.backend.data.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserJpaRepository : JpaRepository<User, Int> {}