package mad.citybin.backend.service

import mad.citybin.backend.data.entity.User
import mad.citybin.backend.data.repository.jpa.RoleJpaRepository
import mad.citybin.backend.data.repository.jpa.UserJpaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService @Autowired constructor(
        val userJpaRepository: UserJpaRepository,
        val roleJpaRepository: RoleJpaRepository
) {

    fun findAll(): Map<String, Any> {
        val result = HashMap<String, Any>()

        val users = userJpaRepository.findAll().toList()
        result["data"] = users

        return result
    }

    fun save(user: User): Map<String, Any> {
        val result = HashMap<String, Any>()
        user.role = roleJpaRepository.findByName("user")
        userJpaRepository.save(user)
        result["data"] = user
        return result
    }

    fun delete(id: Int): Map<String, Any> {
        val result = HashMap<String, Any>()

        val user = userJpaRepository.findById(id).get()

        user.isDeleted = true
        userJpaRepository.save(user)


        result["data"] = user
        return result
    }

}