package mad.citybin.backend.controller.rest.v1

import mad.citybin.backend.data.entity.User
import mad.citybin.backend.service.UserService
import mad.citybin.backend.util.valiidator.isNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
class UserRestControllerV1 @Autowired constructor(
        val userService: UserService
) {

    @GetMapping("")
    fun index(): ResponseEntity<Map<String, Any?>> {
        val responseBody = HashMap<String, Any?>()

        responseBody["name"] = "Hello World"
        responseBody["age"] = 10
        responseBody["data"] = userService.findAll()["data"]

        return ResponseEntity(responseBody, HttpStatus.OK)
    }

    @PostMapping("")
    fun create(@RequestBody requestBody: Map<String, Any?>): ResponseEntity<Map<String, Any?>> {
        val responseBody = HashMap<String, Any?>()

        val user = User()

        requestBody.forEach { (key, value) ->
            when (key) {
                "username" -> {
                    if (isNotNull(value)) user.username = value.toString()
                }
                "email" -> {
                    if (isNotNull(value)) user.email = value.toString()
                }
                "password" -> {
                    if (isNotNull(value)) user.password = value.toString()
                }
                "tel" -> {
                    if (isNotNull(value)) user.tel = value.toString()
                }
            }
        }

        responseBody["data"] = userService.save(user)["data"]

        return ResponseEntity(responseBody, HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): ResponseEntity<Map<String, Any?>> {
        val responseBody = HashMap<String, Any?>()

        responseBody["data"] = userService.delete(id)["data"]

        return ResponseEntity(responseBody, HttpStatus.OK)
    }


}